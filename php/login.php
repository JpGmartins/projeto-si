<?php
$email=$_POST['email'];
$pass=$_POST['password'];
$caminho=$_GET['caminho'];

include 'connectDB.php';

$sql="SELECT funcionario.idFuncionario, funcionario.email, funcionario.pass, especializacao.acesso FROM ((funcionario INNER JOIN funcionario_especializacao ON funcionario.idFuncionario = funcionario_especializacao.idFuncionario) INNER JOIN especializacao ON funcionario_especializacao.idEspecializacao = especializacao.idEspecializacao) WHERE email LIKE '".$email."'";

$result=$conn->query($sql);
$row=$result->fetch_assoc();

//Verificação de Início de Sessão com Hash
if($result->num_rows==1){
    if(password_verify($pass,$row['pass'])){
        session_start();
        $_SESSION['idSessao']=$row['idFuncionario'];
        $_SESSION['acesso']=$row['acesso'];

        if($caminho!=""){
            header('location: '.$caminho);
        }else{
            header('location: ../sistema/pesquisaUtente.php');
        }
    }else{
        header('location: ../index.php?palavraPasseErrada=true');
    }
}else if($result->num_rows>1){
    header('location: ../index.php?emailExiste=true');
}else{
    header('location: ../index.php?contaInexistente=true');
}

?>