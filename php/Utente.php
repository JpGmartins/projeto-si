<?php
    class Utente{
        public $idUtente;
        public $nome;
        public $apelido;
        public $cc;
        public $idFiscal;
        public $dataNascimento;
        public $rua;
        public $localidade;
        public $codPostal;
        public $sexo;

        function __construct($idUtente, $nome, $apelido, $cc,$idFiscal, $dataNascimento, $rua, $localidade, $codPostal, $sexo)
        {
            $this->idUtente=$idUtente;
            $this->nome=$nome;
            $this->apelido=$apelido;
            $this->cc=$cc;
            $this->idFiscal=$idFiscal;
            $this->dataNascimento=$dataNascimento;
            $this->rua=$rua;
            $this->localidade=$localidade;
            $this->codPostal=$codPostal;
            $this->sexo=$sexo;
        }
        public function getIdUtente()
        {
            return $this->idUtente;
        }
        public function setIdUtente($idUtente): void
        {
            $this->idUtente = $idUtente;
        }
        public function getNome()
        {
            return $this->nome;
        }
        public function setNome($nome): void
        {
            $this->nome = $nome;
        }
        public function getApelido()
        {
            return $this->apelido;
        }
        public function setApelido($apelido): void
        {
            $this->apelido = $apelido;
        }
        public function getCc()
        {
            return $this->cc;
        }
        public function setCc($cc): void
        {
            $this->cc = $cc;
        }
        public function getIdFiscal()
        {
            return $this->idFiscal;
        }
        public function setIdFiscal($idFiscal): void
        {
            $this->idFiscal = $idFiscal;
        }
        public function getDataNascimento()
        {
            return $this->dataNascimento;
        }
        public function setDataNascimento($dataNascimento): void
        {
            $this->dataNascimento = $dataNascimento;
        }
        public function getRua()
        {
            return $this->rua;
        }
        public function setRua($rua): void
        {
            $this->rua = $rua;
        }
        public function getLocalidade()
        {
            return $this->localidade;
        }
        public function setLocalidade($localidade): void
        {
            $this->localidade = $localidade;
        }
        public function getCodPostal()
        {
            return $this->codPostal;
        }
        public function setCodPostal($codPostal): void
        {
            $this->codPostal = $codPostal;
        }
        public function getSexo()
        {
            return $this->sexo;
        }
        public function setSexo($sexo): void
        {
            $this->sexo = $sexo;
        }
    }
    session_start();
    $idUtente=$_SESSION['idUtente'];
    include 'connectDB.php';

    $sql = "SELECT * FROM utente WHERE idUtente = " . $idUtente;
    $result = $conn->query($sql);
    $row = mysqli_fetch_assoc($result);

    if ($row['sexo']=="M")
        $aSexo="Masculino";
    else
        $aSexo="Feminino";

    $utente = new Utente($row['idUtente'], $row['nome'], $row['apelido'], $row['cc'], $row['idFiscal'], $row['dataNascimento'], $row['rua'], $row['localidade'], $row['codPostal'], $aSexo);
?>