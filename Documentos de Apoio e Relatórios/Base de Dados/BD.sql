CREATE DATABASE IF NOT EXISTS projeto_si DEFAULT CHARACTER SET utf8;

USE projeto_si;

CREATE TABLE IF NOT EXISTS centroHospitalar(
	idCentroHospitalar INT NOT NULL AUTO_INCREMENT,
	centroHospitalar VARCHAR(255) NOT NULL,
	regiao VARCHAR(50) NOT NULL,
    
    PRIMARY KEY (idCentroHospitalar)
);

CREATE TABLE IF NOT EXISTS servico(
	idServico INT NOT NULL AUTO_INCREMENT,
    servico VARCHAR(255) NOT NULL,
    idCentroHospitalar INT NOT NULL,

	PRIMARY KEY (idServico),
    CONSTRAINT FK_servico_CentroHospitalar FOREIGN KEY (idCentroHospitalar)
    REFERENCES centroHospitalar(idCentroHospitalar)
);

CREATE TABLE IF NOT EXISTS areaClinica(
	idAreaClinica INT NOT NULL AUTO_INCREMENT,
    areaClinica VARCHAR(255) NOT NULL,
    idServico INT NOT NULL,
    
    PRIMARY KEY (idAreaClinica),
    CONSTRAINT FK_servico_areaClinica FOREIGN KEY (idServico) REFERENCES servico(idServico)

);

CREATE TABLE IF NOT EXISTS funcionario(
	idFuncionario INT NOT NULL AUTO_INCREMENT,
    nome VARCHAR(50) NOT NULL,
    apelido VARCHAR(50) NOT NULL,
	cc INT(8) NOT NULL,
    dataNascimento DATE NOT NULL,
	sexo CHAR(1),
	idFiscal INT(9) NOT NULL,
	rua VARCHAR(255),
	localidade VARCHAR(50),
	codPostal VARCHAR(8),
    contratacao DATE,
    email VARCHAR(255) NOT NULL,
    pass VARCHAR(255) NOT NULL,
    
    PRIMARY KEY (idFuncionario)
);

CREATE TABLE IF NOT EXISTS utente(
	idUtente INT NOT NULL,
    nome VARCHAR(50) NOT NULL,
    apelido VARCHAR(50) NOT NULL,
    cc INT(8) NOT NULL,
    idFiscal INT(9),
    dataNascimento DATE NOT NULL,
    rua VARCHAR (255),
    localidade VARCHAR(50),
	codPostal VARCHAR(8),
    sexo CHAR(1),
    
    PRIMARY KEY (idUtente)
);

CREATE TABLE IF NOT EXISTS especializacao(
	idEspecializacao INT NOT NULL AUTO_INCREMENT,
    especializacao VARCHAR(50) NOT NULL,
    grau VARCHAR(50),
    acesso INT(1) NOT NULL,
    
    PRIMARY KEY (idEspecializacao)
);

CREATE TABLE IF NOT EXISTS funcionario_especializacao(
	idFuncionario INT NOT NULL,
    idEspecializacao INT NOT NULL,
    
    PRIMARY KEY (idFuncionario,idEspecializacao),
    CONSTRAINT FK_funcionarioEspecializacao_funcionario FOREIGN KEY (idFuncionario) REFERENCES funcionario(idFuncionario),
    CONSTRAINT FK_funcionarioEspecializacao_especializacao FOREIGN KEY (idEspecializacao) REFERENCES especializacao(idEspecializacao)
);

CREATE TABLE IF NOT EXISTS corpoClinico(
    idCorpoClinico INT NOT NULL AUTO_INCREMENT,
	idFuncionario INT NOT NULL,
    corpoClinico INT NOT NULL,
    
    PRIMARY KEY(idCorpoClinico),
    CONSTRAINT FK_corpoClinico_funcionario FOREIGN KEY (idFuncionario) REFERENCES funcionario(idFuncionario)
);

CREATE TABLE IF NOT EXISTS recurso(
	idRecurso INT NOT NULL AUTO_INCREMENT,
    recurso VARCHAR(50) NOT NULL,
    estado BOOLEAN NOT NULL,
    idCentroHospitalar INT NOT NULL,
    idUtente INT,
    
    PRIMARY KEY (idRecurso),
    CONSTRAINT FK_recurso_centroHospitalar FOREIGN KEY (idCentroHospitalar) REFERENCES centroHospitalar(idCentroHospitalar),
    CONSTRAINT FK_recurso_utente FOREIGN KEY (idUtente) REFERENCES utente(idUtente)
);

CREATE TABLE IF NOT EXISTS consulta(
	idConsulta INT NOT NULL AUTO_INCREMENT,
    idUtente INT NOT NULL,
    idAreaClinica INT NOT NULL,
    dataHora DATETIME NOT NULL,
    notas VARCHAR(255),
    estado VARCHAR(30),
    
    PRIMARY KEY (idConsulta),
    CONSTRAINT FK_consulta_utente FOREIGN KEY (idUtente) REFERENCES utente(idUtente),
    CONSTRAINT FK_consulta_areaClinica FOREIGN KEY (idAreaClinica) REFERENCES areaClinica(idAreaClinica)
);

CREATE TABLE IF NOT EXISTS blocoOperatorio(
	idBlocoOperatorio INT NOT NULL AUTO_INCREMENT,
    blocoOperatorio VARCHAR(50),
    idCentroHospitalar INT NOT NULL,
    
    PRIMARY KEY (idBlocoOperatorio),
    CONSTRAINT FK_blocoOperatorio_centroHospitalar FOREIGN KEY (idCentroHospitalar) REFERENCES centroHospitalar(idCentroHospitalar)
);

CREATE TABLE IF NOT EXISTS cirurgia(
	idCirurgia INT NOT NULL AUTO_INCREMENT,
    idCorpoClinico INT NOT NULL,
    idUtente INT NOT NULL,
    idBlocoOperatorio INT NOT NULL,
    dataHora DATETIME,
    notas VARCHAR(255),
    
    PRIMARY KEY (idCirurgia),
    CONSTRAINT FK_cirurgia_corpoClinico FOREIGN KEY (idCorpoClinico) REFERENCES corpoClinico(idCorpoClinico),
    CONSTRAINT FK_cirurgia_utente FOREIGN KEY (idUtente) REFERENCES utente(idUtente),
    CONSTRAINT FK_cirurgia_blocoOperatorio FOREIGN KEY (idBlocoOperatorio) REFERENCES blocoOperatorio(idBlocoOperatorio)
);

CREATE TABLE IF NOT EXISTS internamento(
	idInternamento INT NOT NULL AUTO_INCREMENT,
    notas VARCHAR(255),
    dataEntrada DATE NOT NULL,
    dataAlta DATE,
    idUtente INT NOT NULL,
    idRecurso INT NOT NULL,

    PRIMARY KEY (idInternamento),
    CONSTRAINT FK_internamento_utente FOREIGN KEY (idUtente) REFERENCES utente(idUtente),
    CONSTRAINT FK_internamento_recurso FOREIGN KEY (idRecurso) REFERENCES recurso(idrecurso)
);

CREATE TABLE IF NOT EXISTS historicoUtente(
    idHistorico INT NOT NULL AUTO_INCREMENT,
    idUtente INT NOT NULL,
    dataRegisto datetime NOT NULL,
    info longtext not null,

    PRIMARY KEY (idHistorico),
    CONSTRAINT FK_historico_utente FOREIGN KEY (idUtente) REFERENCES utente(idUtente)
);

INSERT INTO centroHospitalar (idCentroHospitalar, centroHospitalar, regiao) VALUES
(NULL, 'Centro Hospitalar e Universitário de Coimbra', 'Centro'),
(NULL, 'Centro Hospitalar do Porto', 'Norte'),
(NULL, 'Centro Hospitalar do Algarve', 'Algarve');

INSERT INTO servico (idServico, servico, idCentroHospitalar) VALUES
(NULL, 'Centro de Saúde', '1'),
(NULL, 'Maternidade', '1'),
(NULL, 'Centro Operatório', '1'),
(NULL, 'Centro de Saúde', '2'),
(NULL, 'Maternidade', '2'),
(NULL, 'Centro Operatório', '2'),
(NULL, 'Centro de Saúde', '3'),
(NULL, 'Maternidade', '3'),
(NULL, 'Centro Operatório', '3');

INSERT INTO areaClinica (idAreaClinica, areaClinica, idServico) VALUES 
(NULL, 'Cardiologia', '3'), 
(NULL, 'Pediatria', '2'), 
(NULL, 'Medicina Geral', '1'), 
(NULL, 'Medicina Geral', '4'), 
(NULL, 'Medicina Geral', '7');

INSERT INTO funcionario (idFuncionario, nome, apelido, cc, dataNascimento, sexo, idFiscal, rua, localidade, codPostal, contratacao, email, pass) VALUES
(NULL, 'João Pedro', 'Martins', '15144019', '1996-12-23', 'M', '249919508', 'Rua da Praça N42', 'Souto', '6320-644', '2020-04-02', 'joaopedro.gomes.martins@gmail.com', '$2y$10$7teCG.oJu9PKjYAyKHSsl.KYhzalzxF.5dQNS8.R20n/uA3SjN/Hu'),
(NULL, 'Manuel Joaquim', 'Almeida', '14357319', '1990-03-15', 'M', '247496300', 'Rua das Flores N79', 'Penela', '3002-350', '2020-03-12', 'joaquimanueal@outlook.com', '$2y$10$vYp9DgTlwd5aS8FheZmSSuNtt95sj/X/vFScs8J49LU6YJDbLShEi'),
(NULL, 'Manuela', 'Jacinta', '13456625', '1988-11-03', 'F', '133069870', 'Rua de Trás N5', 'Fundo do Lugar', '1002-630', '2019-06-30', 'manuelajacinta@gmail.com', '$2y$10$vYp9DgTlwd5aS8FheZmSSuNtt95sj/X/vFScs8J49LU6YJDbLShEi');

INSERT INTO utente (idUtente, nome, apelido, cc, idFiscal, dataNascimento, rua, localidade, codPostal, sexo) VALUES
('12345678', 'Maria Rosário', 'Neves', '01423572', '167324968', '1932-02-25', 'Rua Vasconçelos N154-A', 'Coimbra', '3000-024', 'F'),
('216453985', 'Afonso', 'Carvalho', '14326556', '144326980', '1955-05-22', 'Rua do Vale N5', 'Guarda', '3025-687', 'M'),
('933268011', 'António', 'Ramiro', '13788465', '148875325', '1988-10-15', 'Rua da Praça', 'Lisboa', '2001-766', 'M'),
('145558798', 'Jacinta', 'Vaz', '14432008', '177899632', '1953-07-19', 'Rua da Foice', 'Coimbra', '3052-147', 'F');

INSERT INTO especializacao (idEspecializacao, especializacao, grau, acesso) VALUES 
(NULL, 'Cirurgia Geral', 'Doutor',1), 
(NULL, 'Enfermagem', 'Mestre',2), 
(NULL, 'Secretáriado', '12º',3), 
(NULL, 'Auxiliar', '12º',3), 
(NULL, 'Oftlmologia', 'Doutor',1), 
(NULL, 'Imagem Médica', 'Mestre',2);

INSERT INTO funcionario_especializacao (idFuncionario, idEspecializacao) VALUES
('1', '1'),
('2', '3'),
('3', '2');

INSERT INTO blocoOperatorio (idBlocoOperatorio, blocoOperatorio, idCentroHospitalar) VALUES
(NULL,'1','1'),
(NULL,'2','1'),
(NULL,'3','1'),
(NULL,'1','2'),
(NULL,'2','2'),
(NULL,'3','2'),
(NULL,'1','3'),
(NULL, '2','3'),
(NULL,'3','3');

INSERT INTO corpoClinico(idCorpoClinico, idFuncionario, corpoClinico) VALUES
(NULL,'1','1'),
(NULL,'2','1');

INSERT INTO cirurgia(idCirurgia, idCorpoClinico, idUtente, idBlocoOperatorio, dataHora, notas) VALUES
(NULL, '1','216453985','1','2020-06-30 12:00:00','Cirurgia reconstrução facial');

INSERT INTO consulta(idConsulta, idUtente, idAreaClinica, dataHora, notas) VALUES
(NULL, '216453985', '3','2020-07-01 13:00:00', NULL);

INSERT INTO recurso(idRecurso, recurso, estado, idCentroHospitalar, idUtente) VALUES
(NULL,'Maca',false,'1','216453985'),
(NULL,'Cadeira de Rodas', true,'1', NULL);

INSERT INTO internamento(idInternamento, notas, dataEntrada, dataAlta, idUtente, idRecurso) VALUES
(NULL, 'Internamento pós cirúrgico', '2020-06-30', '2020-07-05', '216453985', '1');

INSERT INTO historicoUtente (idHistorico, idUtente, dataRegisto, info) VALUES
(NULL,'216453985', '2020-05-03 12:00:00', 'Entrada no hospital com sintomas de gripe');