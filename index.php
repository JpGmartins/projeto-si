<?php
$caminho=$_GET['caminho'];
session_start();
if(isset($_SESSION['idSessao'])){
    unset ($_SESSION['idSessao']);
}
session_destroy();
?>
<!DOCTYPE html>
<html lang="pt">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Login</title>
        <link rel="icon" href="sistema/img/logo.png">
        <link rel="stylesheet" href="sistema/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="sistema/styleSheet.css">
        <script src="sistema/bootstrap/js/bootstrap.js"></script>
        <style>
            .bd-placeholder-img {
                font-size: 1.125rem;
                text-anchor: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }

            @media (min-width: 768px) {
                .bd-placeholder-img-lg {
                    font-size: 3.5rem;
                }
            }
        </style>
    </head>
    <body class="text-center">
        <?php
            if(isset($_GET['palavraPasseErrada'])){
                echo "<script>alert(\"A palavra passe está errada\")</script>";
            }elseif(isset($_GET['emailExiste'])){
                echo "<script>alert(\"O e-mail não existe\")</script>";
            }elseif(isset($_GET['contaInexistente'])){
                echo "<script>alert(\"A conta não existe\")</script>";
            }
        ?>
        <div id="body">
            <form action="php/login.php?caminho=<?php echo $caminho ?>" method="POST" class="form-signin">
                <img src="sistema/img/logo.png" class="mb-4" width="72" height="72" alt="logo">
                <h1 class="h3 mb-3 font-weight-normal">Efetue o login</h1>
                <label for="email" class="sr-only">E-mail</label>
                <input type="email" name="email" placeholder="email" class="form-control" autofocus required>
                <label for="password" class="sr-only">Password</label>
                <input type="password" name="password" placeholder="Password" class="form-control" required>
                <button type="submit" class="btn btn-lg btn-primary btn-block">Login</button>
            </form>
        </div>
        <div class="container-fluid fixed-bottom">
            <p>Uma produção de João Pedro Martins</p>
            <p>CHad &copy; 2020</p>
        </div>
    </body>
</html>
