<?php
    session_start();
    if(isset($_GET['idUtente'])){
        $idUtente = $_GET['idUtente'];
        $_SESSION['idUtente']=$idUtente;
    }

    $linkActual = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    if(!isset($_SESSION['idSessao'])){
        header('location: ../index.php?caminho='.$linkActual);
    }
    if(!isset($_SESSION['idUtente'])){
        header('location: pesquisaUtente.php');
    }

    include '../php/Utente.php';
    include 'header.php';
?>

<!--Container lateral esquerdo-->
    <div class="container-fluid">
        <div class="row">
            <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a href="historico.php" class="nav-link">Histórico</a>
                    </li>
                    <li class="nav-item">
                        <a href="atualizarDados.php" class="nav-link">Atualizar Dados</a>
                    </li>
                </ul>
            </nav>

<!--Container principal - CONTEÚDO-->
            <main class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
                <div class="d-flex justify-content-between flex-wrap align-items-center pt-3 pb-3 mb-3 border-bottom">
                    <h1><?php echo $utente->getNome()." ".$utente->getApelido()?></h1>
                </div>
                <table class="table table-hover">
                    <tr>
                        <th>CC</th>
                        <td><?php echo $utente->getCc()?></td>
                    </tr>
                    <tr>
                        <th>NIF</th>
                        <td><?php echo $utente->getIdFiscal()?></td>
                    </tr>
                    <tr>
                        <th>Data de Nascimento</th>
                        <td><?php echo $utente->getDataNascimento()?></td>
                    </tr>
                    <tr>
                        <th>Rua</th>
                        <td><?php echo $utente->getRua()?></td>
                    </tr>
                    <tr>
                        <th>Localidade</th>
                        <td><?php echo $utente->getLocalidade()?></td>
                    </tr>
                    <tr>
                        <th>Código Postal</th>
                        <td><?php echo $utente->getCodPostal()?></td>
                    </tr>
                    <tr>
                        <th>Sexo</th>
                        <td><?php echo $utente->getSexo()?></td>
                    </tr>
                </table>
            </main>
        </div>
    </div>
</body>
</html>
