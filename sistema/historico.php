<?php
session_start();

$linkActual = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
if (!isset($_SESSION['idSessao'])) {
    header('location: ../index.php?caminho=' . $linkActual);
}
if (!isset($_SESSION['idUtente'])) {
    header('location: pesquisaUtente.php');
}

include '../php/Utente.php';
include 'header.php';
?>

<!--Container lateral esquerdo-->
<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar">
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a href="novaEntrada.php" class="nav-link">Nova Entrada</a>
                </li>
            </ul>
        </nav>

        <!--Container principal - CONTEÚDO-->
        <main class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <?php
            include 'connectDB.php';
            $sql="SELECT * FROM historicoUtente WHERE idUTENTE LIKE ".$utente->getIdUtente()." ORDER BY dataRegisto ASC";
            $result = $conn->query($sql);
            ?>
            <div class="d-flex justify-content-between flex-wrap align-items-center pt-3 pb-3 mb-3 border-bottom">
                <h1>Histórico de Utente</h1>
            </div>
            <?php
            while ($row=$result->fetch_assoc()){
            ?>
                <h4><?php echo $row['dataRegisto'] ?></h4>
                <p><?php echo $row['info']?></p>
            <?php
            }
            ?>
        </main>
    </div>
</div>
</body>
</html>
