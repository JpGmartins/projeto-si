<?php
session_start();
if(isset($_GET['idUtente'])){
    $idUtente = $_GET['idUtente'];
    $_SESSION['idUtente']=$idUtente;
}

$linkActual = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
if(!isset($_SESSION['idSessao'])){
    header('location: ../index.php?caminho='.$linkActual);
}
if(!isset($_SESSION['idUtente'])){
    header('location: pesquisaUtente.php');
}

include '../php/Utente.php';
include 'header.php';
?>

<!--Container lateral esquerdo-->
<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar">
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a href="historico.php" class="nav-link">Histórico</a>
                </li>
            </ul>
        </nav>

        <!--Container principal - CONTEÚDO-->
        <main class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <div class="d-flex justify-content-between flex-wrap align-items-center pt-3 pb-3 mb-3 border-bottom">
                <h1>Marcação de Consulta</h1>
            </div>
            <form action="../php/marcacaoConsulta.php" method="post">
                <div class="row form-group">
                    <label for="areaClinica" class="col-sm-2 col-form-label">Área Clínica</label>
                    <select name="areaClinica">
                        <?php
                        include '../php/connectDB.php';
                        $sql="SELECT areaClinica.idAreaClinica, areaClinica.areaClinica, servico.servico, centroHospitalar.centroHospitalar FROM ((areaClinica
                        INNER JOIN servico ON areaClinica.idServico=servico.idServico)
                        INNER JOIN centroHospitalar ON servico.idCentroHospitalar=centroHospitalar.idCentroHospitalar)";
                        $result=$conn->query($sql);

                        while ($row=$result->fetch_assoc()){
                            ?>
                            <option value="<?php echo $row['idAreaClinica']?>"><?php echo $row['areaClinica']." - ".$row['servico']." - ".$row['centroHospitalar']?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="row form-group">
                    <label class="col-form-label col-sm-2">Data</label>
                    <input type="datetime-local" placeholder="aaaa-mm-dd hh:mm:ss" name="data" min="<?php echo date('Y-m-d H:i:s') ?>">
                </div>
                <div class="row form-group">
                    <label class="col-form-label col-sm-2">Estado</label>
                    <select name="estado">
                        <option value="Espera">Espera</option>
                        <option value="Exames">Exames</option>
                        <option value="Diagnostico">Diagnóstico</option>
                        <option value="Tratamento">Tratamento</option>
                        <option value="Concluído">Concluído</option>
                    </select>
                </div>
                <div class="row form-group">
                    <label class="col-form-label col-sm-2">Notas</label>
                    <textarea rows="10" cols="10" class="form-control" name="notas"></textarea>
                </div>
                <div class="row form-group">
                    <input type="submit" name="submit" value="OK" class="btn btn-primary">
                </div>
            </form>
        </main>
    </div>
</div>
</body>
</html>
