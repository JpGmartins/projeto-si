<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CHad</title>
    <link rel="icon" href="sistema/img/logo.png">
    <!--<link rel="stylesheet" href="styleSheet.css">-->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <script src="bootstrap/js/bootstrap.js"></script>
</head>
<body>
    <div class="container" style="margin-top: 3%">
        <form action="../php/registarUtente.php" method="POST">
            <h1>Registo de Utente</h1>
            <p>Insira os dados conhecidos</p>
            <div class="form-group row">
                <label for="nome" class="col-sm-2 col-form-label">Nome:</label>
                <input type="text" name="nome" alt="nome" placeholder="Nome" class="form-control col-sm-10">
            </div>
            <div class="form-group row">
                <label for="apelido" class="col-sm-2 col-form-label">Apelido:</label>
                <input type="text" name="apelido" alt="apelido" placeholder="Apelido" class="col-sm-10">
            </div>
            <div class="form-group row">
                <label for="rua" class="col-form-label col-sm-2">Rua</label>
                <input type="text" name="rua" alt="rua" placeholder="Rua" class="form-control col-sm-10">
            </div>
            <div class="form-group row">
                <label class="col-form-label col-sm-2">Localidade</label>
                <input type="text" name="localidade" alt="localidade" class="form-control col-sm-4" placeholder="Localidade">
                <label for="codPostal" class="col-form-label col-sm-2 text-right">Código Postal</label>
                <input type="text" name="codPostal" alt="Código Postal" placeholder="Código Postal" class="form-control col-sm-4">
            </div>
            <div class="form-group row">
                <label class="col-form-label col-sm-2">Data de Nascimento</label>
                <input type="date" name="dataNascimento" alt="Data de Nascimento" class="form-control col-sm-4">
                <label class="col-form-label col-sm-2 text-right">Nº de Utente</label>
                <input type="number" max="999999999" min="10000000" name="idUtente" class="form-control col-sm-4">
            </div>
            <div class="form-group row">
                <label class="col-form-label col-sm-2">Nº Cartão de Cidadão</label>
                <input type="number" name="cc" alt="Cartão de Cidadão" class="form-control col-sm-4" placeholder="Cartão de Cidadão">
                <label class="col-form-label col-sm-2 text-right">NIF</label>
                <input type="number" max="999999999" min="10000000" name="idFiscal" alt="id Fiscal" class="form-control col-sm-4">
            </div>
            <div class="form-group row">
                <label class="col-form-label col-sm-2">Sexo</label>
                <select name="sexo">
                    <option value="M">Masculino</option>
                    <option value="F">Feminino</option>
                    <option value="O">Outro</option>
                </select>
            </div>
            <div class="btn-group">
                <input type="submit" name="submit" class="btn btn-primary">
                <input  type="button" name="voltar" onclick="location.href='pesquisaUtente.php';" alt="voltar" class="btn btn-secondary" value="Voltar">
            </div>
        </form>
    </div>
    <div class="container-fluid text-center">
        <p>Uma produção de João Pedro Martins</p>
        <p>CHad &copy; 2020</p>
    </div>
</body>
</html>