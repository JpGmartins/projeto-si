<?php
session_start();
if (isset($_GET['idUtente'])) {
    $idUtente = $_GET['idUtente'];
    $_SESSION['idUtente'] = $idUtente;
}

$linkActual = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
if (!isset($_SESSION['idSessao'])) {
    header('location: ../index.php?caminho=' . $linkActual);
}
if (!isset($_SESSION['idUtente'])) {
    header('location: pesquisaUtente.php');
}

include '../php/Utente.php';
include 'header.php';
?>

<!--Container lateral esquerdo-->
<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar">
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a href="cirurgias.php" class="nav-link">Cirurgias Agendadas</a>
                </li>
                <li class="nav-item">
                    <a href="novaCirurgia.php" class="nav-link">Nova Cirurgia</a>
                </li>
            </ul>
        </nav>

        <!--Container principal - CONTEÚDO-->
        <main class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <div class="d-flex justify-content-between flex-wrap align-items-center pt-3 pb-3 mb-3 border-bottom">
                <h1>Intervenções Cirurgicas</h1>
            </div>
            <?php
            $sql = "SELECT cirurgia.idCirurgia, centroHospitalar.centroHospitalar, cirurgia.idCorpoClinico, blocoOperatorio.blocoOperatorio, cirurgia.dataHora, cirurgia.notas FROM ((cirurgia
	INNER JOIN blocoOperatorio ON cirurgia.idBlocoOperatorio=blocoOperatorio.idBlocoOperatorio)
    INNER JOIN centroHospitalar ON blocoOperatorio.idCentroHospitalar=centroHospitalar.idCentroHospitalar)
    WHERE cirurgia.idUtente LIKE " . $utente->getIdUtente();
            include '../php/connectDB.php';

            $result = $conn->query($sql);

            if($result->num_rows>0){
                while ($row=$result->fetch_assoc()){
                    ?>
                    <table class="table table-hover mt-5">
                        <tr>
                            <th>Centro Hospitalar</th>
                            <td><?php echo $row['centroHospitalar'] ?></td>
                        </tr>
                        <tr>
                            <th>Bloco Operatório</th>
                            <td><?php echo $row['blocoOperatorio'] ?></td>
                        </tr>
                        <tr>
                            <th>Corpo Clínico</th>
                            <td><a href="corpoClinico.php?idCorpoClinico=<?php echo $row['idCorpoClinico']?>"><?php echo $row['idCorpoClinico'] ?></a></td>
                        </tr>
                        <tr>
                            <th>Data - Hora</th>
                            <td><?php echo $row['dataHora'] ?></td>
                        </tr>
                        <tr>
                            <th>Notas</th>
                            <td><?php echo $row['notas'] ?></td>
                        </tr>
                    </table>
                    <input type="button" name="Editar" value="Editar" onclick=location.href='alterarCirurgia.php?idCirurgia=<?php echo $row['idCirurgia'] ?>'>
                    <?php
                }
            }

            ?>

        </main>
    </div>
</div>
</body>
</html>
