<?php
session_start();
if (isset($_GET['idUtente'])) {
    $idUtente = $_GET['idUtente'];
    $_SESSION['idUtente'] = $idUtente;
}

$linkActual = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
if (!isset($_SESSION['idSessao'])) {
    header('location: ../index.php?caminho=' . $linkActual);
}
if (!isset($_SESSION['idUtente'])) {
    header('location: pesquisaUtente.php');
}

include '../php/Utente.php';
include 'header.php';
?>

<!--Container lateral esquerdo-->
<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar">
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a href="internamento.php" class="nav-link">Internamentos Agendados</a>
                </li>
                <li class="nav-item">
                    <a href="agendarInternamento.php" class="nav-link">Agendar Internamento</a>
                </li>
            </ul>
        </nav>

        <!--Container principal - CONTEÚDO-->
        <main class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <div class="d-flex justify-content-between flex-wrap align-items-center pt-3 pb-3 mb-3 border-bottom">
                <h1>Internamento</h1>
            </div>
            <?php
            $sql = "SELECT internamento.notas, internamento.dataEntrada, internamento.dataAlta, utente.nome, utente.apelido, recurso.recurso, internamento.idRecurso, centroHospitalar.centroHospitalar FROM (((internamento
INNER JOIN utente ON internamento.idUtente=utente.idUtente)
INNER JOIN recurso ON internamento.idRecurso=recurso.idRecurso)
INNER JOIN centroHospitalar ON recurso.idCentroHospitalar=centroHospitalar.idCentroHospitalar)
    WHERE internamento.idUtente LIKE " . $utente->getIdUtente();
            include '../php/connectDB.php';

            $result = $conn->query($sql);

            if($result->num_rows>0){
                while ($row=$result->fetch_assoc()){
                    ?>
                    <table class="table table-hover mt-5">
                        <tr>
                            <th>Data de Entrada</th>
                            <td><?php echo $row['dataEntrada'] ?></td>
                        </tr>
                        <tr>
                            <th>Data de Alta</th>
                            <td><?php echo $row['dataAlta'] ?></td>
                        </tr>
                        <tr>
                            <th>Recurso</th>
                            <td><?php echo $row['recurso']." - ".$row['idRecurso'] ?></td>
                        </tr>
                        <tr>
                            <th>Centro Hospitalar</th>
                            <td><?php echo $row['centroHospitalar']?></td>
                        </tr>
                        <tr>
                            <th>Notas</th>
                            <td><?php echo $row['notas'] ?></td>
                        </tr>
                    </table>
                    <input type="button" name="Editar" value="Editar" onclick=location.href='alterarCirurgia.php?idCirurgia=<?php echo $row['idCirurgia'] ?>'>
                    <?php
                }
            }
            ?>

        </main>
    </div>
</div>
</body>
</html>
