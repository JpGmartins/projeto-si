<?php
session_start();
if (isset($_GET['idUtente'])) {
    $idUtente = $_GET['idUtente'];
    $_SESSION['idUtente'] = $idUtente;
}

$linkActual = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
if (!isset($_SESSION['idSessao'])) {
    header('location: ../index.php?caminho=' . $linkActual);
}
if (!isset($_SESSION['idUtente'])) {
    header('location: pesquisaUtente.php');
}

include '../php/Utente.php';
include 'header.php';
?>

<!--Container lateral esquerdo-->
<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar">
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a href="historico.php" class="nav-link">Histórico</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">Atualizar Dados</a>
                </li>
            </ul>
        </nav>

        <!--Container principal - CONTEÚDO-->
        <main class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <div class="d-flex justify-content-between flex-wrap align-items-center pt-3 pb-3 mb-3 border-bottom">
                <h1><?php echo $utente->getNome() . " " . $utente->getApelido() ?></h1>
            </div>
            <form action="../php/atualizarDados.php" method="post">
                <table class="table table-hover">
                    <tr>
                        <th>Número de Utente</th>
                        <td><input type="number" name="idUtente" value="<?php echo $utente->getIdUtente()?>" readonly></td>
                    </tr>
                    <tr>
                        <th>CC</th>
                        <td><input type="text" name="cc" value="<?php echo $utente->getCc() ?>"></td>
                    </tr>
                    <tr>
                        <th>NIF</th>
                        <td><input type="text" name="nif" value="<?php echo $utente->getIdFiscal() ?>"></td>
                    </tr>
                    <tr>
                        <th>Data de Nascimento</th>
                        <td><input type="date" name="dataNascimento" value="<?php echo $utente->getDataNascimento() ?>">
                        </td>
                    </tr>
                    <tr>
                        <th>Rua</th>
                        <td><input type="text" name="rua" value="<?php echo $utente->getRua() ?>"></td>
                    </tr>
                    <tr>
                        <th>Localidade</th>
                        <td><input type="text" name="localidade" value="<?php echo $utente->getLocalidade() ?>"></td>
                    </tr>
                    <tr>
                        <th>Código Postal</th>
                        <td><input type="text" name="codPostal" value="<?php echo $utente->getCodPostal() ?>"></td>
                    </tr>
                    <tr>
                        <th>Sexo</th>
                        <td>
                            <select name="sexo">
                                <option value="M">Masculino</option>
                                <option value="F">Feminino</option>
                            </select>
                        </td>
                        <!--<td><input type="text" name="sexo" value="<?php// echo $utente->getSexo() ?>"></td>-->
                    </tr>
                </table>
                <input type="submit" class="btn btn-primary" value="Atualizar">
            </form>
        </main>
    </div>
</div>
</body>
</html>

