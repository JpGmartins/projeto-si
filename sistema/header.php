<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>CHad - Pesquisa Utente</title>
    <link rel="icon" href="img/logo.png">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="styleSheet.css">
    <script src="bootstrap/js/bootstrap.js"></script>
</head>
<body>
<!--Container superior - MENUS-->
<div class="container-fluid" style="background-color: #563d7c">
    <div class="row col-12">
        <img src="img/logo.png" alt="logo" width="72" height="72">
        <ul class="nav mt-4">
            <li class="nav-item">
                <a class="nav-link text-white active" href="home.php">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" href="marcacoes.php">Marcações</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" href="consultas.php">Consultas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" href="cirurgias.php">Cirurgias</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" href="internamento.php">Internamento</a>
            </li>
        </ul>
        <div class="align-items-end">
            <ul class="nav mt-4">
                <li class="nav-item">
                    <a class="nav-link text-white" href="pesquisaUtente.php">Novo Utente</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white" href="../index.php">Logout</a>
                </li>
            </ul>
        </div>
    </div>
</div>