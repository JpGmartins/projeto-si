<?php
session_start();

$linkActual = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
if (!isset($_SESSION['idSessao'])) {
    header('location: ../index.php?caminho=' . $linkActual);
}
if (!isset($_SESSION['idUtente'])) {
    header('location: pesquisaUtente.php');
}

include '../php/Utente.php';
include 'header.php';
?>

<!--Container lateral esquerdo-->
<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar">
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a href="marcacoes.php" class="nav-link">Marcar Consulta</a>
                </li>
            </ul>
        </nav>

        <!--Container principal - CONTEÚDO-->
        <main class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <div class="d-flex justify-content-between flex-wrap align-items-center pt-3 pb-3 mb-3 border-bottom">
                <h1>Consultas</h1>
            </div>
            <?php
            include '../php/connectDB.php';
            $sql="SELECT consulta.idConsulta, areaClinica.areaClinica, servico.servico, centroHospitalar.centroHospitalar, consulta.dataHora, consulta.notas, consulta.estado FROM (((consulta
            INNER JOIN areaClinica ON consulta.idAreaClinica=areaClinica.idAreaClinica)
            INNER JOIN servico ON areaClinica.idServico=servico.idServico)
            INNER JOIN centroHospitalar ON servico.idCentroHospitalar=centroHospitalar.idCentroHospitalar)
            WHERE idUtente LIKE ".$utente->getIdUtente()." ORDER BY consulta.dataHora DESC";

            $result=$conn->query($sql);
            if($result->num_rows>0){
                while ($row=$result->fetch_assoc()){
                    ?>
                    <table class="table table-hover" style="margin-top: 5%">
                        <tr>
                            <th>Area Clínica</th>
                            <td><?php echo $row['areaClinica']?></td>
                        </tr>
                        <tr>
                            <th>Serviço</th>
                            <td><?php echo $row['servico'] ?></td>
                        </tr>
                        <tr>
                            <th>Centro Hospitalar</th>
                            <td><?php echo $row['centroHospitalar']?></td>
                        </tr>
                        <tr>
                            <th>Data - Hora</th>
                            <td><?php echo $row['dataHora'] ?></td>
                        </tr>
                        <tr>
                            <th>Notas</th>
                            <td><?php echo $row['notas'] ?></td>
                        </tr>
                        <tr>
                            <th>Estado</th>
                            <td><?php echo $row['estado'] ?></td>
                        </tr>
                    </table>
                    <input type="button" name="selecionar" value="Alterar" onclick=location.href='alterarConsulta.php?idConsulta=<?php echo $row['idConsulta'] ?>'>
                    <?php
                }
            }else{
                echo "<p>Não existem consultas marcadas para este utente.</p>";
            }
            ?>
        </main>
    </div>
</div>
</body>
</html>
