<?php
    session_start();
    if(!isset($_SESSION['idSessao'])){
        header('location: ../index.php');
    }
?>
<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>CHad - Pesquisa Utente</title>
    <link rel="icon" href="sistema/img/logo.png">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="styleSheet.css">
    <script src="bootstrap/js/bootstrap.js"></script>
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
</head>
<body>
    <div class="container" style="margin-top: 5%">
        <form action="selecaoUtente.php" method="post">
            <h1 class="h3 mb-3 font-weight-normal" style="text-align: center">Pesquisa de Utente</h1>
            <div class="form-group row">
                <label for="idUtente" class="col-sm-2 col-form-label">Número de Utente</label>
                <input type="number" name="idUtente" placeholder="Nº Utente" class="form-control col-sm-4">
                <label for="cc" class="col-sm-2 col-form-label" style="text-align: right">Cartão de Cidadão</label>
                <input type="number" class="col-sm-4 form-control" placeholder="Cartão de Cidadão" name="cc">
            </div>
            <div class="form-group row">
                <label for="nome" class="col-sm-2 col-form-label">Nome</label>
                <input type="text" name="nome" placeholder="Nome" class="form-control col-sm-10">
            </div>
            <div class="form-group row">
                <label for="apelido" class="col-sm-2 col-form-label">Apelido</label>
                <input type="text" name="apelido" placeholder="Apelido" class="form-control col-sm-10">
            </div>
            <div class="form-group row">
                <label for="idFiscal" class="col-sm-2 col-form-label">NIF</label>
                <input type="number" name="idFiscal" placeholder="NIF" class="form-control col-sm-4">
                <label for="localidade" class="col-form-label col-sm-2" style="text-align: right">Localidade</label>
                <input type="text" name="localidade" class="col-sm-4 form-control" placeholder="Localidade">
            </div>
            <div class="form-group row">
                <label for="dataNascimento" class="col-sm-2 col-form-label">Data de Nascimento</label>
                <input type="date" name="dataNascimento" class="col-sm-4 form-control">
            </div>
            <div class="btn-group row">
                <button name="submit" class="btn btn-primary" type="submit">Pesquisar</button>
                <button type="button" name="registar" onclick=location.href='registarUtente.php' class="btn btn-secondary">Registar Utente</button>
                <button name="reset" class="btn btn-secondary" type="reset">Repor</button>
            </div>
        </form>
    </div>
    <div class="container-fluid fixed-bottom text-center">
        <p>Uma produção de João Pedro Martins</p>
        <p>CHad &copy; 2020</p>
    </div>
</body>
</html>