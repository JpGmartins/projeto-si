<?php
session_start();
if (isset($_GET['idUtente'])) {
    $idUtente = $_GET['idUtente'];
    $_SESSION['idUtente'] = $idUtente;
}

$linkActual = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
if (!isset($_SESSION['idSessao'])) {
    header('location: ../index.php?caminho=' . $linkActual);
}
if (!isset($_SESSION['idUtente'])) {
    header('location: pesquisaUtente.php');
}

include '../php/Utente.php';
include 'header.php';

$idCorpoClinico=$_GET['idCorpoClinico'];
?>

<!--Container lateral esquerdo-->
<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar">
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a href="cirurgias.php" class="nav-link">Cirurgias Agendadas</a>
                </li>
                <li class="nav-item">
                    <a href="novaCirurgia.php" class="nav-link">Nova Cirurgia</a>
                </li>
            </ul>
        </nav>

        <!--Container principal - CONTEÚDO-->
        <main class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <div class="d-flex justify-content-between flex-wrap align-items-center pt-3 pb-3 mb-3 border-bottom">
                <h1>Corpo Clínico</h1>
            </div>
            <?php
            $sql = "SELECT corpoClinico.corpoClinico, funcionario.nome, funcionario.apelido, especializacao.especializacao FROM (((corpoClinico
	INNER JOIN funcionario ON corpoClinico.idFuncionario=funcionario.idFuncionario)
    INNER JOIN funcionario_especializacao ON funcionario_especializacao.idFuncionario=funcionario.idFuncionario)
	INNER JOIN especializacao ON especializacao.idEspecializacao=funcionario_especializacao.idEspecializacao)
	WHERE corpoClinico.corpoClinico LIKE ".$idCorpoClinico;
            include '../php/connectDB.php';

            $result = $conn->query($sql);

            if($result->num_rows>0){

                ?><table class="table table-hover mt-5">
                <tr>
                    <th>Corpo Clínico</th>
                    <th>Nome</th>
                    <th>Apelido</th>
                    <th>Especialização</th>
                </tr><?php
                while ($row=$result->fetch_assoc()){
                    ?>
                    <tr>
                        <td><?php echo $row['corpoClinico']?></td>
                        <td><?php echo $row['nome']?></td>
                        <td><?php echo $row['apelido']?></td>
                        <td><?php echo $row['especializacao']?></td>
                    </tr>
                    <?php
                }
                ?></table><?php
            }

            ?>

        </main>
    </div>
</div>
</body>
</html>