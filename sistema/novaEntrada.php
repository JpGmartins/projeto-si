<?php
session_start();

$linkActual = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
if (!isset($_SESSION['idSessao'])) {
    header('location: ../index.php?caminho=' . $linkActual);
}
if (!isset($_SESSION['idUtente'])) {
    header('location: pesquisaUtente.php');
}

include '../php/Utente.php';
include 'header.php';
?>

<!--Container lateral esquerdo-->
<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar">
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a href="#" class="nav-link">Nova Entrada</a>
                </li>
            </ul>
        </nav>

        <!--Container principal - CONTEÚDO-->
        <main class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <div class="d-flex justify-content-between flex-wrap align-items-center pt-3 pb-3 mb-3 border-bottom">
                <h1>Nova Entrada</h1>
            </div>
            <form action="../php/novaEntrada.php" method="post">

                <div class="form-group row">
                    <label for="idUtente" class="col-sm-2 col-form-label">Número de Utente</label>
                    <input type="number" name="idUtente" value="<?php echo $utente->getIdUtente()?>" class="form-control col-sm-4" readonly>
                </div>
                <div class="form-group row">
                    <h2 class="h2">Informações</h2>
                    <textarea name="info" cols="50" rows="10" class="form-control"></textarea>
                </div>

                <div class="form-group row">
                    <input type="submit" name="submit" value="Submit" class="btn-primary btn">
                </div>
            </form>
        </main>
    </div>
</div>
</body>
</html>
