<?php
    $idUtente=$_POST['idUtente'];
    $cc=$_POST['cc'];
    $nome=$_POST['nome'];
    $apelido=$_POST['apelido'];
    $idFiscal=$_POST['idFiscal'];
    $localidade=$_POST['localidade'];
    $dataNascimento=$_POST['dataNascimento'];
    $sql="SELECT idUtente, cc, nome, apelido, idFiscal, localidade, dataNascimento FROM utente
        WHERE
        idUtente LIKE '$idUtente'
        OR cc LIKE '$cc'
        OR nome like '$nome'
        OR apelido LIKE '$apelido'
        OR idFiscal LIKE '$idFiscal'
        OR localidade LIKE '$localidade'
        OR dataNascimento LIKE '$dataNascimento'";
    
    include '../php/connectDB.php';
    $result=$conn->query($sql);
    
?>
<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>CHad - Selecionar Utente</title>
    <link rel="icon" href="sistema/img/logo.png">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <script src="bootstrap/js/bootstrap.js"></script>
    <style>
        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
        th{
            width: 25%;
        }

    </style>
</head>
<body>
    <div class="container">
        <h1 class="h3 mb-3 font-weight-normal" style="margin-top: 5%">Resultados da Pesquisa</h1>
        <?php
        if($result->num_rows>0){
            while($row=$result->fetch_assoc()){
                ?>
                <table class="table table-hover" style="margin-top: 5%">
                    <tr>
                        <th>Nº Utente</th>
                        <td><?php echo $row['idUtente']?></td>
                    </tr>
                    <tr>
                        <th>CC</th>
                        <td><?php echo $row['cc'] ?></td>
                    </tr>
                    <tr>
                        <th>Nome</th>
                        <td><?php echo $row['nome']." ".$row['apelido'] ?></td>
                    </tr>
                    <tr>
                        <th>Localidade</th>
                        <td><?php echo $row['localidade'] ?></td>
                    </tr>
                    <tr>
                        <th>Data de Nascimento</th>
                        <td><?php echo $row['dataNascimento'] ?></td>
                    </tr>
                </table>
            <input type="button" name="selecionar" value="Selecionar" onclick=location.href='home.php?idUtente=<?php echo $row['idUtente'] ?>'>
            <?php
            }
            ?>
            <div class="text-center container-fluid">
                <p>Uma produção de João Pedro Martins</p>
                <p>CHad &copy; 2020</p>
            </div>
            <?php
        }else{?>
            <p>Nenhum registo encontrado</p>
            <div class="btn-group row">
                <input type="button" name="registar" value="Registar Utente" onclick=location.href='registarUtente.php' class="btn btn-primary">
                <input type="button" name="voltar" value="Voltar" onclick=location.href='pesquisaUtente.php' class="btn btn-secondary">
            </div>
            <div class="text-center container-fluid fixed-bottom">
                <p>Uma produção de João Pedro Martins</p>
                <p>CHad &copy; 2020</p>
            </div>
        <?php }
        ?>
    </div>

</body>
</html>
