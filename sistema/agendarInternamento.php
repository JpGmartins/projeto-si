<?php
session_start();
if (isset($_GET['idUtente'])) {
    $idUtente = $_GET['idUtente'];
    $_SESSION['idUtente'] = $idUtente;
}

$linkActual = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
if (!isset($_SESSION['idSessao'])) {
    header('location: ../index.php?caminho=' . $linkActual);
}
if (!isset($_SESSION['idUtente'])) {
    header('location: pesquisaUtente.php');
}

include '../php/Utente.php';
include 'header.php';

$idCirurgia = $_GET['idCirurgia'];
?>

<!--Container lateral esquerdo-->
<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar">
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a href="internamento.php" class="nav-link">Internamentos Agendados</a>
                </li>
                <li class="nav-item">
                    <a href="agendarInternamento.php" class="nav-link">Agendar Internamento</a>
                </li>
            </ul>
        </nav>

        <!--Container principal - CONTEÚDO-->
        <main class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <div class="d-flex justify-content-between flex-wrap align-items-center pt-3 pb-3 mb-3 border-bottom">
                <h1>Marcação de Cirurgia</h1>
            </div>

            <form action="../php/agendarInternamento.php" method="post">
                <div class="row form-group">
                    <label class="col-form-label col-sm-2">Data de Entrada</label>
                    <input type="date" name="dataEntrada" min="<?php echo date('Y-m-d H:i:s') ?>">
                </div>
                <div class="row form-group">
                    <label class="col-form-label col-sm-2">Data de Alta Prevista</label>
                    <input type="date" name="dataAlta" min="<?php echo date('Y-m-d H:i:s') ?>">
                </div>
                <div class="row form-group">
                    <label for="recurso" class="col-sm-2 col-form-label">Recurso</label>
                    <select name="recurso">
                        <?php
                        include '../php/connectDB.php';
                        $sql = "SELECT recurso.idRecurso, recurso.recurso, centroHospitalar.centroHospitalar FROM (recurso
INNER JOIN centroHospitalar ON recurso.idCentroHospitalar=centroHospitalar.idCentroHospitalar)";
                        $result = $conn->query($sql);
                        while ($row = $result->fetch_assoc()) {
                            ?>
                            <option value="<?php echo $row['idRecurso'] ?>"><?php echo $row['recurso']." - ".$row['centroHospitalar']?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="row form-group">
                    <label class="col-form-label col-sm-2">Notas</label>
                    <textarea name="notas" class="form-control"></textarea>
                </div>
                <div class="row form-group">
                    <input type="submit" name="submit" value="OK" class="btn btn-primary">
                </div>
            </form>
        </main>
    </div>
</div>
</body>
</html>
