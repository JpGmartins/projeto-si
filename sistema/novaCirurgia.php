<?php
session_start();
if (isset($_GET['idUtente'])) {
    $idUtente = $_GET['idUtente'];
    $_SESSION['idUtente'] = $idUtente;
}

$linkActual = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
if (!isset($_SESSION['idSessao'])) {
    header('location: ../index.php?caminho=' . $linkActual);
}
if (!isset($_SESSION['idUtente'])) {
    header('location: pesquisaUtente.php');
}

include '../php/Utente.php';
include 'header.php';

$idCirurgia = $_GET['idCirurgia'];
?>

<!--Container lateral esquerdo-->
<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar">
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a href="cirurgias.php" class="nav-link">Cirurgias Agendadas</a>
                </li>
                <li class="nav-item">
                    <a href="novaCirurgia.php" class="nav-link">Nova Cirurgia</a>
                </li>
            </ul>
        </nav>

        <!--Container principal - CONTEÚDO-->
        <main class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <div class="d-flex justify-content-between flex-wrap align-items-center pt-3 pb-3 mb-3 border-bottom">
                <h1>Marcação de Cirurgia</h1>
            </div>

            <form action="../php/marcacaoCirurgia.php" method="post">
                <div class="row form-group">
                    <label for="blocoOperatorio" class="col-sm-2 col-form-label">Bloco Operatório</label>
                    <select name="blocoOperatorio">
                        <?php
                        include '../php/connectDB.php';
                        $sql = "SELECT blocoOperatorio.idBlocoOperatorio, blocoOperatorio.blocoOperatorio, centroHospitalar.centroHospitalar from (blocoOperatorio
INNER JOIN centroHospitalar ON blocoOperatorio.idCentroHospitalar=centroHospitalar.idCentroHospitalar)";
                        $result = $conn->query($sql);
                        while ($row = $result->fetch_assoc()) {
                            ?>
                            <option value="<?php echo $row['idBlocoOperatorio'] ?>"><?php echo $row['blocoOperatorio'] . " - " . $row['centroHospitalar']?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="row form-group">
                    <label for="corpoClinico" class="col-sm-2 col-form-label">Corpo Clínico</label>
                    <select name="corpoClinico">
                        <?php
                        include '../php/connectDB.php';
                        $sql = "SELECT idCorpoClinico, corpoClinico FROM corpoClinico";
                        $result = $conn->query($sql);
                        while ($row = $result->fetch_assoc()) {
                            ?>
                            <option value="<?php echo $row['idCorpoClinico'] ?>"><?php echo $row['corpoClinico']?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>

                <div class="row form-group">
                    <label class="col-form-label col-sm-2">Data</label>
                    <input type="datetime-local" name="data" min="<?php echo date('Y-m-d H:i:s') ?>">
                </div>
                <div class="row form-group">
                    <label class="col-form-label col-sm-2">Notas</label>
                    <textarea rows="10" cols="10" class="form-control" name="notas"></textarea>
                </div>
                <div class="row form-group">
                    <input type="submit" name="submit" value="OK" class="btn btn-primary">
                </div>
            </form>
        </main>
    </div>
</div>
</body>
</html>
