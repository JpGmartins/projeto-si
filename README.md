# Projeto-SI
Projeto final de Sistemas de Informação 2019-2020

## Observações Gerais
Na pasta "Documentos de Apoio e Relatórios" serão incluídos tos os ficheiros que fazem parte da assistência ao projeto como o relatório final, esquemas de base de dados e SQL, casos de uso, ficheiros de imagem em edição (exemplo do Photoshop), tipo de letra etc.

## Dia 26-02-2020
Criação do Repositório e Inserção dos casos de uso existentes.

##Dia 02 de Adril de 2020
Modelação Provisória da Base de Dados, dos seus diagramas e levantamento de entidades.

##Dia 23 de Abril de 2020
Alteração da base de dados da tabela utente, adicionando atributos para rua, localidade etc.
Página de login do funcionário
Página de procura e registo de utente

##Dia 11 de maio de 2020
Criados a página que apresenta os resultados da pesquisa de Utente, a página de registo de utente, o ficheiro de conexão à base de dados.